import React, { Component } from 'react'
import '../../src/index.css'

class Buttons extends Component {
    render() {
        const { value, className, onClick} = this.props;
        return(
            <button value = {value} className = {`calc-button ${className}`} onClick = {onClick}>
                {value}
            </button>
        );
    }
}

export default Buttons