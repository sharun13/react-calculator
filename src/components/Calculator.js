import React, { Component } from 'react';
import Display from './Display';
import Buttons from './Buttons';
import '../../src/index.css';

class Calculator extends Component {

    constructor(props) {
        super(props)

        this.state = {
            number: '0',
            operator: '',
            result: '0'
        }

        this.handleNumberInput = this.handleNumberInput.bind(this);
        this.handleOperatorInput = this.handleOperatorInput.bind(this);
        this.handleEqualInput = this.handleEqualInput.bind(this);
        this.handleDotInput = this.handleDotInput.bind(this);
        this.handleClearInput = this.handleClearInput.bind(this);
        this.handleNegativeInput = this.handleNegativeInput.bind(this);

    }

    handleNumberInput(event) {
        event.preventDefault();
        const value = event.target.innerText;

        // Appending the value of the pressed button to the number
        let returnObj = {}
        this.setState((prevState) => {
            if (prevState.number === '0' && value === '0') { // prevents zeroes at the beginning
                returnObj.number = '0';
            } else if (prevState.number % 1 === 0 && value !== '0') { // checking if it is a whole number
                returnObj.number = Number(prevState.number + value); // it returns a Number() to get rid of 0's in the front when a number is typed 
            } else {
                returnObj.number = prevState.number + value; // also includes value !== "0" so we can type a 0 right after the dot
            }

            if (prevState.operator) { // this line resets the result if a new number was typed with no operator stored
                returnObj.result = prevState.result; 
            } else {
                returnObj.result = '0';
            }

            return returnObj;

        });
    }

    handleOperatorInput(event) {
        const operation = event.target.innerText;
        this.setState((prevState) => {
            return {
                operator: operation,
                result: prevState.number ? prevState.number : prevState.result,
                number: operation === '-' && prevState.number === '0' ? '-' : ''
            }
        });
    }

    handleEqualInput() {
        let finalResult = 0;
        switch (this.state.operator) {
            case '/':
                finalResult = this.state.result / this.state.number;
                break;
            case 'X':
                finalResult = this.state.result * this.state.number;
                break;
            case '-':
                finalResult = this.state.result - this.state.number;
                break;
            case '+':
                finalResult = Number(this.state.result) + Number(this.state.number);
                break;
            default:
                finalResult = this.state.number ? this.state.number : this.state.result;
        }
        this.setState({
            number: '',
            operator: '',
            result: Number(finalResult)
        });
    }

    handleDotInput() {
        // concats "." to current number if number doesn't include one already
        if (this.state.number) {
            this.setState({
                number: !this.state.number.toString().includes('.') ? this.state.number + '.' : this.state.number
            });
        }
    }

    handleClearInput() {
        this.setState({
            number: '0',
            operator: '',
            result: '0'
        });
    }

    handleNegativeInput() {
        // special case: number is empty and result isn't (e.g. after equal operation) it inverts the result
        if (!this.state.number && this.state.result) {
            this.setState({
                result: -this.state.result
            })
        // default : negates the number
        } else {
            this.setState({
                number: -this.state.number
            })
        }
    }

    render() {
        const { number, operator, result } = this.state;
        return (
            <div className='main-container'>
                <Display output = {number ? number : result + operator}></Display>
                <div className='button-container'>
                    <Buttons value = "C" className = "clear" onClick = {this.handleClearInput}></Buttons>
                    <Buttons value = "+-" className = "operation" onClick = {this.handleNegativeInput}></Buttons>
                    <Buttons value = "/" className = "operation" onClick = {this.handleOperatorInput}></Buttons>
                    <Buttons value = "7" className = "number" onClick = {this.handleNumberInput}></Buttons>
                    <Buttons value = "8" className = "number" onClick = {this.handleNumberInput}></Buttons>
                    <Buttons value = "9" className = "number" onClick = {this.handleNumberInput}></Buttons>
                    <Buttons value = "X" className = "operation" onClick = {this.handleOperatorInput}></Buttons>
                    <Buttons value = "4" className = "number" onClick = {this.handleNumberInput}></Buttons>
                    <Buttons value = "5" className = "number" onClick = {this.handleNumberInput}></Buttons>
                    <Buttons value = "6" className = "number" onClick = {this.handleNumberInput}></Buttons>
                    <Buttons value = "-" className = "operation" onClick = {this.handleOperatorInput}></Buttons>
                    <Buttons value = "1" className = "number" onClick = {this.handleNumberInput}></Buttons>
                    <Buttons value = "2" className = "number" onClick = {this.handleNumberInput}></Buttons>
                    <Buttons value = "3" className = "number" onClick = {this.handleNumberInput}></Buttons>
                    <Buttons value = "+" className = "operation" onClick = {this.handleOperatorInput}></Buttons>
                    <Buttons value = "0" className = "number" onClick = {this.handleNumberInput}></Buttons>
                    <Buttons value = "." className = "number" onClick = {this.handleDotInput}></Buttons>
                    <Buttons value = "=" className = "equal" onClick = {this.handleEqualInput}></Buttons>
                </div>
            </div>
        )
    }
}

export default Calculator