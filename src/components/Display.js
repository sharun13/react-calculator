import React, { Component } from 'react'
import { Textfit } from 'react-textfit'
import '../../src/index.css'

class Display extends Component {
    render() {
        const { output } = this.props;
        return (
            <Textfit className='display' max={80} mode="single"> {output} </Textfit>
            )
    }
}

export default Display