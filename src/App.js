import './App.css';
import '../src/index.css';
import Calculator from './components/Calculator';

function App() {
  return (
    <div className="App">
      <Calculator></Calculator>
    </div>
  );
}

export default App;
